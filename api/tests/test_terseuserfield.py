from rest_framework.serializers import ValidationError
from api.tests.util import ZpravaAPITestCase
from api.serializers import TerseUserField
from api.models import User


class TestTerseUserField(ZpravaAPITestCase):

    fixtures = ['user_conversation_messages']

    def setUp(self):
        self.field = TerseUserField()
        self.usera = User.objects.get(username='UserA')
        self.userb = User.objects.get(username='UserB')
        self.userc = User.objects.get(username='UserC')

    def test_just_id(self):
        obj = self.field.to_internal_value({
            'id': 3,
        })
        self.assertEqual(obj, self.userc)

    def test_just_username(self):
        obj = self.field.to_internal_value({
            'username': 'UserC',
        })
        self.assertEqual(obj, self.userc)

    def test_inexact_username(self):
        obj = self.field.to_internal_value({
            'username': 'UsERA',
        })
        self.assertEqual(obj, self.usera)

    def test_mismatch_user(self):
        with self.assertRaises(ValidationError):
            self.field.to_internal_value({
                'username': 'UserA',
                'id': 3,
            })

    def test_invalid_id(self):
        with self.assertRaises(ValidationError):
            self.field.to_internal_value({
                'id': 500,
            })

    def test_invalid_username(self):
        with self.assertRaises(ValidationError):
            self.field.to_internal_value({
                'username': 'INVALIDUSER',
            })

    def test_valid_username_nonexistant_id(self):
        with self.assertRaises(ValidationError):
            self.field.to_internal_value({
                'username': 'UserC',
                'id': 500,
            })

    def test_valid_id_nonexistant_username(self):
        with self.assertRaises(ValidationError):
            self.field.to_internal_value({
                'username': 'INVALIDUSER',
                'id': 3,
            })
